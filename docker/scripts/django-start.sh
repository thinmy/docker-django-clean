#!/usr/bin/env bash

sleep 7
python3 /src/app/manage.py collectstatic --noinput -c
python3 /src/app/manage.py runserver 0.0.0.0:8000
